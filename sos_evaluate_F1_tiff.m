function [ F1 ] = sos_evaluate_F1_tiff( ground_path, test_path )
% Takes inputs of paths to tiff stacks and returns the F1 voxel score.
% Arguement 'ground_path' - string containing path to ground truth
% classifications.
% Arguement 'test_path' - string containing path to predicted
% classifications.



ground = sos_read_data(ground_path);
test = sos_read_data(test_path);

[TP, FP, TN, FN] = sos_evaluate_classification_metrics(ground,test);

if (TP == 0)
    F1 = 0;
else
    precision = TP / (TP + FP);
    recall = TP / (TP + FN);

    F1 = 2 * precision * recall / (precision + recall);    
end



end

