function [ stack ] = sos_read_data(path)
% Reads a multi-page TIFF file and returns it as a 3-D double stack.

% Input - relative path from which to read.
% Output - 3-D image stack.

info = imfinfo(path);
num_images = numel(info);
A = [];
for k = 1:num_images
    A = imread(path, k);
    stack(:,:,k) = A;
end
stack = double(stack) / 255;

end