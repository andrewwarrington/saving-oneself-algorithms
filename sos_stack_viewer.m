function sos_stack_viewer(mat_stack)
% Simple viewer for visualising image stacks. Use mouse wheel to scroll.

% Input - Image stack as a 3-D matrix.

   warning('off','images:initSize:adjustingMag');

   num = size(mat_stack,3);
   k = 1;
   f = figure('WindowScrollWheelFcn',@figScroll,'Name','Stack Visualizer');
   title('Stack Visualizer')
   imshow(mat_stack(:,:,k));


   function figScroll(src,callbackdata)
      if callbackdata.VerticalScrollCount > 0 
          if k <= num
            k = k+1;              
            imshow(mat_stack(:,:,k));
            title(strcat('Z = ',num2str(k)));
          end
      elseif callbackdata.VerticalScrollCount < 0 
          if k > 1
            k = k - 1;              
            imshow(mat_stack(:,:,k));
            title(strcat('Z = ',num2str(k)));
          end
      end
    end % figScroll
   
end % scroll_wheel   


