#!/bin/bash
#PBS -N DEMO
#PBS -M andrew.warrington@keble.ox.ac.uk
#PBS -m abe
#PBS -q parallel


# Set output and error directories
#PBS -o Reports/results_${PBS_JOBID}.out
#PBS -e Reports/results_${PBS_JOBID}.err

# Ensure job was launched correctly in PBS -o directory.
#echo $par_1 

# Launch matlab and change directory to folder containing matlab script, then run. 
# Parameter is passed in as a string and hence str2num must be applied inside matlab script.
/usr/local/bin/matlab -nodesktop -nodisplay -nosplash -noFigureWindows -r "cd '$PBS_O_WORKDIR' ; do_stuff_matlab() ;"
